"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


#global val
speedlimits = [(1000, 13.333, 26), (600, 11.428, 28), (400, 15, 30), (200, 15, 32), (0, 15, 34)]


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    
    timer = 0
    if int(control_dist_km)>((brevet_dist_km) ):
      return False 

    for dist,minspeed,maxspeed in speedlimits:
      if control_dist_km > dist:
        timer +=(control_dist_km - dist)/maxspeed
        control_dist_km = dist

    timer = round(timer,3)
    #return timer

    return arrow.get(brevet_start_time).shift(hours = timer).isoformat()
    


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    
    timer = 0
    close_time = arrow.get(brevet_start_time)
    for dist,minspeed,maxspeed in speedlimits:
      if control_dist_km > dist:
        timer +=(control_dist_km - dist)/minspeed
        control_dist_km = dist
    timer = round(timer,3)

    return arrow.get(brevet_start_time).shift(hours = timer).isoformat()



#testing

"""
def main():

  print(open_time(150,200,"2008-09-15T00:00:00"))
  #print(close_time(150,200,"2008-09-15T00:00:00"))
  #print("\n")
  #print(open_time(100,200,"2008-09-15T00:00:00"))
  #print(close_time(100,200,"2008-09-15T00:00:00"))

if __name__ == "__main__":
  main()



def test_acp():
  assert open_time(150,200,"2008-09-15T00:00:00")=="2008-09-15T04:24:43.200000+00:00"
"""